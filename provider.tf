provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "NewAIProject"
  region      = "us-central1"
}